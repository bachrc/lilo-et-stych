use chrono::NaiveDate;
use serde_derive::Deserialize;
use serde_derive::Serialize;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ReponseStych {
    pub rows_point_de_cours: Vec<RowsPointDeCour>,
    pub rows_proposition: Vec<RowsProposition>,
    pub rows_moniteur: Vec<RowsMoniteur>,
    pub rows_nb_credit_used: Vec<RowsNbCreditUsed>,
    pub planning_timer: i64,
    pub nb_credit_selected: i64,
    pub nb_credit_available: i64,
    pub id_type_cours_searched: i64,
    pub type_cours_searched: String,
    pub flash_message: String,
    pub read_only: i64,
    pub statut: String,
    pub action: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RowsPointDeCour {
    #[serde(rename = "id_liste_adresse_cours")]
    pub id_liste_adresse_cours: String,
    #[serde(rename = "code_postal")]
    pub code_postal: String,
    pub ville: String,
    pub intitule: String,
    pub adresse: String,
    #[serde(rename = "adresse_cp_ville")]
    pub adresse_cp_ville: String,
    #[serde(rename = "id_ville")]
    pub id_ville: String,
    pub latitude: String,
    pub longitude: String,
    #[serde(rename = "dep_id")]
    pub dep_id: String,
    #[serde(rename = "dep_code")]
    pub dep_code: String,
    #[serde(rename = "dep_libelle")]
    pub dep_libelle: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RowsProposition {
    #[serde(rename = "id_user")]
    pub id_user: String,
    #[serde(rename = "info_date")]
    pub info_date: NaiveDate,
    #[serde(rename = "heure_debut")]
    pub heure_debut: String,
    #[serde(rename = "heure_fin")]
    pub heure_fin: String,
    #[serde(rename = "ids_lac_possible")]
    pub ids_lac_possible: Vec<String>,
    #[serde(rename = "has_type_cours_from_dispo")]
    pub has_type_cours_from_dispo: String,
    #[serde(rename = "nb_heure")]
    pub nb_heure: f64,
    #[serde(rename = "id_jour")]
    pub id_jour: String,
    #[serde(rename = "id_lac")]
    pub id_lac: String,
    pub moniteur: String,
    #[serde(rename = "heure_debut_fr")]
    pub heure_debut_fr: String,
    #[serde(rename = "heure_fin_fr")]
    pub heure_fin_fr: String,
    #[serde(rename = "nb_credit")]
    pub nb_credit: i64,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RowsMoniteur {
    #[serde(rename = "id_utilisateur")]
    pub id_utilisateur: String,
    pub prenom: String,
    #[serde(rename = "initiale_nom")]
    pub initiale_nom: String,
    pub moyenne: i64,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RowsNbCreditUsed {
    #[serde(rename = "info_date")]
    pub info_date: String,
    #[serde(rename = "nb_credit")]
    pub nb_credit: i64,
}
