mod response;

use response::{ReponseStych, RowsProposition};

#[tokio::main]
async fn main() {
    let client = reqwest::Client::new();
    let response = client
        .get("https://www.stych.fr/elearning/planning-conduite/get-planning-proposition")
        .header("Cookie", "PHPSESSID=prout")
        .send()
        .await
        .expect("BAH OUAIS MAIS FAUT ENVOYER A UN MOMENT")
        .json::<ReponseStych>()
        .await
        .expect("EXPECT EXPECT EXPECT");

    let id_moniteurice = "951444";

    let propositions_de_notre_moniteurice: Vec<&RowsProposition> = response
        .rows_proposition
        .iter()
        .filter(|it| it.id_user == id_moniteurice)
        .collect();

    let premiere_dispo = propositions_de_notre_moniteurice
        .iter()
        .min_by_key(|it| it.info_date)
        .expect("ELLE DOIT ETRE DISPO");

    println!(
        "Salut les potos ! Il y a {} propositions ! Dont {} de notre monitrice",
        response.rows_proposition.len(),
        propositions_de_notre_moniteurice.len()
    );

    println!(
        "Le plus tôt que notre monitrice est dispo est {}",
        premiere_dispo.info_date.format("%d %m %Y")
    )
}
